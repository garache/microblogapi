module microblog

go 1.15

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/go-chi/chi v1.5.4
	github.com/joho/godotenv v1.4.0
	github.com/lib/pq v1.10.5
	golang.org/x/crypto v0.0.0-20220408190544-5352b0902921
)
